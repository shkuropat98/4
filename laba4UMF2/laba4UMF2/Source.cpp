#define _CRT_SECURE_NO_WARNINGS
#include <clocale>
#include <fstream>
#include <iostream>
#include <vector>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

using namespace std;

vector<vector<double>> operator+(const vector<vector<double>>& matrix, const vector<vector<double>>& matrix2) {
	vector<vector<double>> c;
	int size = matrix.size();
	c.resize(size);
	for (auto& vect : c) {
		vect.resize(size);
	}
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			c[i][j] = matrix[i][j] + matrix2[i][j];
		}
	}
	return c;
}

vector<vector<double>> operator*(const double& a, const vector<vector<double>>& matrix) {
	vector<vector<double>> c;
	int size = matrix.size();
	c.resize(size);
	for (auto& vect : c) {
		vect.resize(size);
	}
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			c[i][j] = matrix[i][j] * a;
		}
	}
	return c;
}

vector<double> operator*(const vector<vector<double>>& matrix, const vector<double>& vect) {
	vector<double> c;
	int size = matrix.size();
	c.resize(size);

	for (int i = 0; i < size; ++i) {
		double sum = 0;
		for (int j = 0; j < size; ++j) {
			sum += matrix[i][j] * vect[j];
		}
		c[i] = sum;
	}
	return c;
}

// ��������� ���������������� ������� �� ������ ��� ������ ��������������� �������
vector<double> operator%(const vector<vector<double>>& matrix, const vector<double>& vect)
{
	vector<double> c;
	int size = matrix.size();
	c.resize(size);

	for (int i = 0; i < size; ++i) {
		double sum = 0;
		for (int j = 0; j < size; ++j) {
			sum += matrix[j][i] * vect[j];
		}
		c[i] = sum;
	}
	return c;
}

vector<double> operator* (const double& c, const vector<double>& vect)
{
	vector<double> result = vect;
	for (int i = 0; i < result.size(); i++)
	{
		result[i] = vect[i] * c;
	}
	return result;
}

//  ��������� ��������� ������� �� ������
double  operator* (const vector<double>& vector1, const vector<double>& vector2)
{
	double result = 0;
	for (int i = 0; i < vector1.size(); i++)
	{
		result+=(vector1[i] * vector2[i]);
	}
	return result;
}

vector<double> operator+(const vector<double>& vect1, const vector<double>& vect2) {
	vector<double> c;
	int size = vect1.size();
	c.resize(size);

	for (int i = 0; i < size; ++i) {
		c[i] = vect1[i] + vect2[i];
	}
	return c;
}

vector<double> operator-(const vector<double>& vect1, const vector<double>& vect2) {
	vector<double> c;
	int size = vect1.size();
	c.resize(size);

	for (int i = 0; i < size; ++i) {
		c[i] = vect1[i] - vect2[i];
	}
	return c;
}

void Read_matrix(string matrix_filename, vector<vector<double>>& matrix_A, int Size)
{
	fstream file_matrix;
	file_matrix.open(matrix_filename);
	if (file_matrix.is_open())
	{
		for (int i = 0; i < Size; i++)
		{
			for (int j = 0; j < Size; j++)
			{
				file_matrix >> matrix_A[i][j];
			}
		}
	}
	file_matrix.close();
}
void Read_vector(string vector_filename, vector<double>& vector, int Size)
{
	fstream file_vector;
	file_vector.open(vector_filename);
	if (file_vector.is_open())
	{
		for (int i = 0; i < Size; i++)
		{
			file_vector >> vector[i];
		}
	}
	file_vector.close();
}

double ScalVectOnVect(vector<double> vector1, vector<double> vector2, int Size)
{
	int N = vector1.size();
	double sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += (vector1[i] * vector2[i]);
	}
	return sum;
}

vector<double> LOS(vector<vector<double>> matrix_A, vector<double> vector_B, int Size)  // ��� ����� ������� A
{
	double epsilon = 1e-13;
	vector<double> r0(Size);
	vector<double> r_next(Size);
	vector<double> z0(Size);
	vector<double> z_next(Size);
	vector<double> x_next(Size);
	vector<double> p0(Size);
	vector<double> p_next(Size);
	vector<double> x0(Size);
	vector<double> result = x0;
	
	double normF;
	double sum = 0;
	double alpha = 0;
	int iter_count = 0;

	int max_iter = 10000;

	auto t1 = Clock::now();
	double normRkkvad = 0;
	r0 = vector_B - matrix_A * result;
	z0 = r0;
	p0 = matrix_A * z0;

	// ����� ����� ����� ���
	FILE* out;
	out = fopen("LOSout.txt", "wt");
	fprintf(out, "Iter  x                           y                           z                               NormRk\n");
	fprintf(out, "%d;\t%2.18le;\t%2.18le;\t%2.18le;\t%2.18le;\n", iter_count, x0[0], x0[1], x0[2], normRkkvad);
	// ����� ����� ����� ���

	do
	{
		iter_count++;
		//alpha nach
		double alphaChisl = 0;
		double alphaZnam = 0;
		alphaChisl = ScalVectOnVect(p0, r0, Size);
		alphaZnam = ScalVectOnVect(p0, p0, Size);

		alpha = alphaChisl / alphaZnam;
		//alpha kon

		x_next = result + alpha * z0;

		//rk
		r_next = r0 - alpha * p0;

		normRkkvad = ScalVectOnVect(r_next, r_next, Size);


		//betta nach
		double bettaChisl = 0;
		double bettaZnam = 0;
		double betta = 0;
		vector<double> temp;
		temp = matrix_A * r_next;

		bettaZnam = ScalVectOnVect(p0, p0, Size);
		bettaChisl = ScalVectOnVect(p0, temp, Size);

		betta = -(bettaChisl / bettaZnam);
		//betta kon

		//zk
		z_next = r_next + betta * z0;


		p_next = matrix_A * r_next + betta * p0;


		result = x_next;
		r0 = r_next;
		z0 = z_next;
		p0 = p_next;
		fprintf(out, "%d;\t%2.18le;\t%2.18le;\t%2.18le;\t%2.18le;\n", iter_count, result[0], result[1], result[2], normRkkvad);
	} while (normRkkvad > epsilon && iter_count < max_iter);
	auto t2 = Clock::now();
	cout << "iter_count LOS: " << iter_count << "\n";
	fclose(out);
	std::cout << "LOS Delta t: "
		<< std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
		<< " microseconds" << std::endl;
	return result;
}

vector<double> BCG(vector<vector<double>> matrix_A, vector<double> vector_B, int size) {

	// prev values
	vector<double> x0(size);
	vector<double> r0(size);
	vector<double> p0(size);
	vector<double> z0(size);
	vector<double> s0(size);

	vector<double> xk(size);
	vector<double> rk(size);
	vector<double> pk(size);
	vector<double> zk(size);
	vector<double> sk(size);

	double alpha;
	double alpha_chisl;
	double alpha_znam;
	double betta;

	vector<double> Az0;



	r0 = vector_B - (matrix_A * x0);
	p0 = r0;
	z0 = r0;
	s0 = r0;
	int iter_count = 0;
	double epsilon = 1e-13;
	int max_iter = 100000;
	double normRkkvad = 0;
	// ����� ����� ����� ���
	FILE* out;
	out = fopen("BSGout.txt", "wt");
	fprintf(out, "Iter  x                           y                           z                               NormRk\n");
	fprintf(out, "%d;\t%2.18le;\t%2.18le;\t%2.18le;\t%2.18le;\n", iter_count, x0[0], x0[1], x0[2], normRkkvad);
	// ����� ����� ����� ���
	auto t1 = Clock::now();
	do
	{
		iter_count++;
		Az0 = matrix_A * z0;

		alpha_chisl = p0 * r0;
		alpha_znam = s0 * Az0;
		alpha = alpha_chisl / alpha_znam;

		xk = x0 + (alpha * z0);

		rk = r0 - (alpha * (Az0));
		normRkkvad = rk * rk;

		pk = p0 - (alpha * (matrix_A % s0));

		//betta = (pk * rk) / (p0 * r0);
		betta = (pk * rk) / alpha_chisl;

		zk = rk + (betta * z0);

		sk = pk + (betta * s0);


		x0 = xk;
		r0 = rk;
		p0 = pk;
		z0 = zk;
		s0 = sk;
		fprintf(out, "%d;\t%2.18le;\t%2.18le;\t%2.18le;\t%2.18le;\n", iter_count, xk[0], xk[1], xk[2], normRkkvad);
	} while (normRkkvad > epsilon && iter_count < max_iter);

	auto t2 = Clock::now();
	cout << "iter_count BSG: " << iter_count << "\n";
	fclose(out);
	std::cout << "BSG Delta t: "
		<< std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
		<< " microseconds" << std::endl;
	return xk;
}

void main() 
{
	
	int size = 3;

	vector<vector<double>> matrix_A;
	matrix_A.resize(size);
	for (int i = 0; i < matrix_A.size(); i++)
	{
		matrix_A[i].resize(size);
	}

	vector<double> vector_B(size);

	Read_matrix("matrix_A.txt", matrix_A, size);
	Read_vector("vector_B.txt", vector_B, size);

	
	vector<double> result(size);

	result = BCG(matrix_A, vector_B, size);
	printf("Result BCG\n");
	for (int i = 0; i < result.size(); i++)
	{		
		printf("%2.18le ", result[i]);
	}
	printf("\n\n");

	result = LOS(matrix_A, vector_B, size);
	printf("Result LOS\n");
	for (int i = 0; i < result.size(); i++)
	{		
		printf("%2.18le ", result[i]);
	}
	printf("\n\n");
	

	int k = 3;

}